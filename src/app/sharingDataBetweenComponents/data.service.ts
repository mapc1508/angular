import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  // behavior subjects are used to share data between unrelated components (like siblings)
  // it ensures that data used by the same service provider is in sync between the components that consume it
  private messageSource = new BehaviorSubject<string>('default message'); // holds the current value of the message
  currentMessage = this.messageSource.asObservable(); // this variable will be used by the components to subscribe

  constructor() { }

  // calls the next() on behavioralSubject to change its current value
  changeMessage(message: string) {
    this.messageSource.next(message);
  }
}
