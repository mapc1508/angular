import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  constructor(private data: DataService) { }

  message: string;

  ngOnInit() {
    // subscribing to the Observable in the dataService
    this.data.currentMessage.subscribe(m => this.message = m);
  }

  newMessage(message: string) {
    // changing current value of the message in the service
    this.data.changeMessage(message);
  }

}
