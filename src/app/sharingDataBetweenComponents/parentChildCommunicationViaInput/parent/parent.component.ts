import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  constructor(private data: DataService) { }

  message: string;

  ngOnInit() {
    // subscribing to the Observable in the dataService
    this.data.currentMessage.subscribe(m => this.message = m);
  }

}
