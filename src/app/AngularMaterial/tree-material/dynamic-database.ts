import { DynamicFlatNode } from './dynamic-flat-node';

/**
 * Database for dynamic data. When expanding a node in the tree, the data source will need to fetch
 * the descendants data from the database.
 */
export class DynamicDatabase {
    dataMap = new Map([
      ['Fruits', ['Apple', 'Orange', 'Banana']],
      ['Vegetables', ['Tomato', 'Potato', 'Onion']],
      ['Apple', ['Fuji', 'Macintosh']],
      ['Onion', ['Yellow', 'White', 'Purple']]
    ]);

    rootLevelNodes = ['Fruits', 'Vegetables'];

    /** Initial data from database */
    initialData(): DynamicFlatNode[] {
      return this.rootLevelNodes.map(name => new DynamicFlatNode(name, 0, true));
    }

    getChildren(node: string): string[] | undefined {
      return this.dataMap.get(node);
    }

    isExpandable(node: string): boolean {
      return this.dataMap.has(node);
    }
  }
  /**
   * File database, it can build a tree structured Json object from string.
   * Each node in Json object represents a file or a directory. For a file, it has filename and type.
   * For a directory, it has filename and children (a list of files or directories).
   * The input will be a json object string, and the output is a list of `FileNode` with nested
   * structure.
   */
