import { Component, Injectable } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { CollectionViewer, SelectionChange } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { map } from 'rxjs/operators';
import { DynamicDataSource } from './dynamic-database.service';
import { DynamicFlatNode } from './dynamic-flat-node';
import { DynamicDatabase } from './dynamic-database';




/**
 * @title Tree with dynamic data
 */
@Component({
  selector: 'tree-material',
  templateUrl: 'tree-material.component.html',
  styleUrls: ['tree-material.component.css'],
  providers: [DynamicDatabase]
})
export class TreeMaterialComponent {

  treeControl: FlatTreeControl<DynamicFlatNode>;

  dataSource: DynamicDataSource;

  constructor(database: DynamicDatabase) {
    this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new DynamicDataSource(this.treeControl, database);

    this.dataSource.data = database.initialData();
  }

  getLevel = (node: DynamicFlatNode) => { return node.level; };

  isExpandable = (node: DynamicFlatNode) => { return node.expandable; };

  hasChild = (_: number, _nodeData: DynamicFlatNode) => { return _nodeData.expandable; };
}
