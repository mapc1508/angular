import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ChildComponent } from './sharingDataBetweenComponents/parentChildCommunicationViaInput/child/child.component';
import { ParentComponent } from './sharingDataBetweenComponents/parentChildCommunicationViaInput/parent/parent.component';
import { DataService } from './sharingDataBetweenComponents/data.service';
import { InputMaterialComponent } from './AngularMaterial/input-material/input-material.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatTreeModule } from '@angular/material/tree';
import { TreeMaterialComponent } from './AngularMaterial/tree-material/tree-material.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@NgModule({
  declarations: [
    AppComponent,
    ChildComponent,
    ParentComponent,
    InputMaterialComponent,
    TreeMaterialComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatInputModule,
    MatTreeModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
